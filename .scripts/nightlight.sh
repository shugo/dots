# redshift -c ~/.config/redshift/redshift.conf
while getopts a: flag
do
    case "${flag}" in
        a) argu=${OPTARG};;
    esac
done
# echo "argument: $argu";
if [[ $argu -eq 1 ]]; 
then
    # starting redshift
    killall nightlight
    redshift -c ~/.config/redshift/redshift.conf
else
    # killing redshift
    killall nightlight
fi
