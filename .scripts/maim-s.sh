#!/bin/bash

maim -s ~/lib/pictures/Screenshots/$(date +%m-%d-%H:%M:%S).png
notify-send 'Screenshot taken'
