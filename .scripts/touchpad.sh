#!/bin/bash

touchpadid=` xinput | grep Elan | awk '{print $6}' | awk '{ print substr($1,4) }' `
tappingid=`xinput list-props $touchpadid | grep -m 1 'Tapping Enabled' | awk {' print substr($4,2,3) '}`
naturalscrollingid=`xinput list-props $touchpadid | grep -m 1 'Natural Scrolling' | awk {' print substr($5,2,3) '}`

xinput set-prop $touchpadid $tappingid 1
xinput set-prop $touchpadid $naturalscrollingid 1
xsetroot -cursor_name left_ptr
