from datetime import datetime
import pytz

# list of desired countries
Country_Zones = ['America/New_York', 'Asia/Kolkata', 'Europe/Dublin', 
                 'Europe/Paris', 'US/Alaska', 'US/Arizona', 'US/Central', 
                'US/East-Indiana']

country_time_zones = []
for country_time_zone in Country_Zones:
    country_time_zones.append(pytz.timezone(country_time_zone))
for i in range(len(country_time_zones)):
    country_time = datetime.now(country_time_zones[i])
    print(f"{country_time.strftime('%d')} {country_time.strftime('%H:%M')} for {Country_Zones[i]}")
