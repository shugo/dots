#!/usr/bin/env python3
import subprocess
import math
import time

# set distance (hotcorner sensitivity)
radius = 20

# top-left, top-right, bottom-left, bottom-right
commands = [
    None,
    None,
    "subl",
    "sh ~/automation/next-workspace.sh",
    ]

def get(cmd):
    return subprocess.check_output(cmd).decode("utf-8").strip()

def get_pos():
    return [int(s.split(":")[1]) for s in get(["xdotool", "getmouselocation"]).split()[:2]]

# get the resolution
scrdata = get("xrandr").split(); resindex = scrdata.index("connected")+2
res = [int(n) for n in scrdata[resindex].split("+")[0].split("x")]
# list the corners, could be more elegant no doubt
corners = [[0, 0], [res[0], 0], [0, res[1]], [res[0], res[1]]]

match1 = None

while True:
    time.sleep(0)
    pos = get_pos()
    # get the current difference from the mousepointer to each of the corner (radius)
    diff = [int(math.sqrt(sum([(c[i]-pos[i])**2 for i, n in enumerate(res)])))\
            for c in corners]
    # see if any of the corners is "approached" within the radius
    match2 = [diff.index(n) for n in diff if n < radius]
    # if so, and the corresponding command is not set to None, run it.
    if all([match2 != match1, match2]):
        cmd = commands[match2[0]]
        if cmd:
            subprocess.Popen(["/bin/bash", "-c", cmd])
    match1 = match2