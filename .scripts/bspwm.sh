#!/bin/bash

# scripts
~/.scripts/gesture.sh
~/.scripts/touchpad.sh

# polkit
lxpolkit &

# utilities
picom --config ~/.config/picom.conf &
sxhkd -c ~/.config/sxhkd/sxhkdrc &
feh --bg-scale ~/lib/pictures/wall/good/dark.jpg &
#feh --bg-scale ~/lib/pictures/Screenshots/study/class-11.png
dunst -conf ~/.config/dunst/dunstrc &
tint2 &
clipmenud &
xss-lock -- i3lock -n -i ~/lib/pictures/wall/good/lock.png &

# tray-items
cbatticon &
nm-applet &

xrdb ~/.Xresources
