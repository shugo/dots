import subprocess
import os

a = subprocess.getoutput(
    'diff -q -s ~/.config/kitty/kitty.light ~/.config/kitty/kitty.conf')
res = a.find("differ")

if res != -1:
    print("Current Terminal Colour Scheme: Dark")
    os.system("truncate -s 0 ~/.config/kitty/kitty.conf")
    os.system("cat ~/.config/kitty/kitty.light > ~/.config/kitty/kitty.conf")
    print("Terminal Colour Scheme Changed to Light")
else:
    print("Current Terminal Colour Scheme: Light")
    os.system("truncate -s 0 ~/.config/kitty/kitty.conf")
    os.system("cat ~/.config/kitty/kitty.dark > ~/.config/kitty/kitty.conf")
    print("Terminal Colour Scheme Changed to Dark")
