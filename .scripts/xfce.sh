#!/bin/bash

# scripts
~/.scripts/gesture.sh
~/.scripts/touchpad.sh

# polkit
#lxpolkit &

# utilities
#picom --config ~/.config/picom.conf &
sxhkd -c ~/.config/sxhkd/sxhkdrc &
#feh --bg-scale ~/lib/pictures/wall/fucklife.jpg &
#dunst -conf ~/.config/dunst/dunstrc &
#xfce4-panel -d &
clipmenud &
#xss-lock -- i3lock -n -i ~/lib/pictures/wall/good/lock.png &

# tray-items
cbatticon &
#nm-applet &

