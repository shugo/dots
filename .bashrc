# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

# completion with sudo
complete -cf sudo

# alias
alias ranger='env TERM=xterm-kitty ranger'
alias ls='ls --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias cls="clear && printf '\e[3J'"
alias wrt='echo nothing happened'
alias irc='irssi'
alias vi='nvim'
alias dockerflutter='docker run -it -v /home/blue/Documents/computer/projects:/home/dev/projects/flut arch_devel'
alias emacs='XLIB_SKIP_ARGB_VISUALS=1 emacs'
alias sshugo='mosh shugo@ctrl-c.club'
alias sshaks='ssh -p 61141 aks@192.168.0.107'
# alias fixwifi='sudo service network-manager restart'
alias fixwifi='sudo systemctl restart NetworkManager'
alias xup='xrdb ~/.Xresources'
alias icat='kitty +kitten icat'
alias makepdf='convert \*.jpg'
alias sudovi='sudo -e'
alias cpick="colorpicker --short --one-shot --preview"

# temp aliases
alias rad='docker run -it arch_devel'
alias gta='cd ~/dev/games/gtavc/ && ./reVC'
alias debian_server='docker run -itp 4567:80 -v "/home/blue/src/projects/local":/var/www/html/local/ debian_server'
alias lofi='mpv --shuffle --ytdl-format=139 https://youtube.com/playlist?list=PL6NdkXsPL07IOu1AZ2Y2lGNYfjDStyT6O'
alias ythigh='mpv --ytdl-format=22 $1'
alias ytmed='mpv --ytdl-format=18 $1' 
alias ytlow='mpv --ytdl-format=395 $1'
alias ytmp3='mpv --ytdl-format=139 $1'
alias sshfs_envs='sshfs shugo@envs.net: ~/src/env/envs-mnt/'

PS1="\e[1;13m\w\e[0m\n$ "
HISTIGNORE="ls:cls:startx:neofetch*:sshugo*:xup*:gta*:fixwifi*:cmus*:docker:battery:cd ~:sudo apt update:sudo apt upgrade:exit:htop:top:free*:l:ll:cd ~/.all/*:cd ~/.all*:cd .all*:irc:wc:"

# env variables
ANDROID_SDK_ROOT=$HOME/.android/sdk
ANDROID_HOME=$HOME/.android/sdk
LANG="en_US.UTF-8"
export LANG
export PATH="$HOME/.android/sdk/platform-tools:$PATH"
export PATH="$HOME/.android/sdk/emulator:$PATH"
export PATH="$HOME/.android/sdk/cmdline-tools/tools/bin:$PATH"
export PATH="$PATH:/$HOME/devel/flutter/bin"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH=$PATH:/usr/local/go/bin
export GOPATH=$HOME/src/go
export PATH="$HOME/.emacs.d/bin:$PATH"
export TERM=xterm-256color
export CM_LAUNCHER=rofi
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# startup terminal cmds
#cat ~/.all/him/routine.txt;
#cat ~/.all/him/rem.txt;
#python3 ~/.scripts/daystogo.py

# zsh like suggestion
#bind 'set show-all-if-ambiguous on'
#bind 'TAB:menu-complete'

# >>>> Vagrant command completion (start)
#. /opt/vagrant/embedded/gems/2.2.19/gems/vagrant-2.2.18/contrib/bash/completion.sh
# <<<<  Vagrant command completion (end)

# Eternal bash history.
# ---------------------
# Undocumented feature which sets the size to "unlimited".
# http://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"


# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
